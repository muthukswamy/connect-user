'use strict';

var assert = require('assert');
const auth = require('../src/auth');
const bcrypt = require('bcrypt');

// Mocks
var next = () => {return true};
var res = {
    status : (a) => {return {
        send: (b) => {return false}
    }}
};

// Tests
describe('Authentication', () => {
    it('It should generate password hash', (done) => {
        var password = 'Hello123!';
        var localHash = bcrypt.hashSync(password, 10);
        auth.encryptPassword(password, (error, hash) => {
            assert.equal(bcrypt.compareSync(password, hash), true);
            done();
        });
    });

    it('It should compare password hash', (done) => {
        var password = 'Hello123!';
        var localHash = bcrypt.hashSync(password, 10);
        auth.comparePassword(password, localHash, (error, result) => {
            assert.equal(result, true);

            auth.comparePassword('another password', localHash, (error, result) => {
                assert.equal(result, false);
                done();
            });
        });
    });

    it('It should check user session', () => {
        var result1 = auth.checkUser({
            session: {
                user: 'user@email.com'
            }
        }, res, next);
        var result2 = auth.checkUser({}, res, next);

        assert.equal(result1, true);
        assert.equal(result2, false);
    });

    it('It should check admin session', () => {
        var result1 = auth.checkAdmin({
            session: {
                user: 'user@email.com',
                isAdmin: true
            }
        }, res, next);
        var result2 = auth.checkAdmin({
            session: {
                user: 'user@email.com'
            }
        }, res, next);

        assert.equal(result1, true);
        assert.equal(result2, false);
    });
});
