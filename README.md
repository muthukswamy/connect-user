# Connect User
An application to manage users and their connections. Application uses ExpressJS as web framework and Neo4j as database as users and their connections are the core aspect of this project. Neo4j databse is a graph database which is ideal for the given scenario where there will be multiple cross connections between the users of the application. It is better performing than traditional RDBMS systems when it comes to querying relationships.

Whole application has been split up into two docker images, one containing the web server and the other with database server. This is to make way for load balancing in the future where multiple web server images can be spawned as required. All the docker images are launched and managed by docker compose which starts the application with just one command.

## Requirements
* Docker with docker compose
* NodeJS v8.9 or higher

## Installation & Usage
Clone this repository and run ```docker-compose up``` to start the Neo4j image and the web app image. The API endpoints will be available in ```localhost:3000```. API endpoints can be consumed by a frontend app or can be used in an API tester application directly.

## Endpoints
All post endpoints require JSON inputs and get endpoints require inputs in URL parameters. User sessions are handled by cookies so when testing the endpoints a system which handles cookies is required. A suggested testing tool is Postman(https://www.getpostman.com/)

### Register ```POST: /register```
Registers a new user into the system. Requires the fields name, email and password in the JSON format sent to it as post request. *Admin user* to register an admin user, set the name as "admin". This is a temporary setup to allow for admin registartion in this demo.

### Login ```POST: /login```
Login as an user/admin in the system. Requires email and password fields as JSON in the post request. This sets up a session in the system and sends back a session cookie. Use this cookie in subsequent requests to make authenticated requests.

### Logout ```GET: /logout```
Destroys the current session.

### List users ```GET: /list-users```
An authenticated endpoint which lists all the users except for the current logged in user.

### Connect with user ```POST: /connect-user```
An authenticated endpoint which creates a connection between the current user and the provided user. The user to follow should be supplied as an JSON object with email field in the post request.

### Disconnect from user ```POST: /disconnect-user```
Call this endpoint same as the connect-user endpoint. This endpoint removes the connection between the current user and the provided user.

### List connections for user [Admin only] ```GET: /user-connections```
An admin only authenticated end point. Gets a list of followers for the given user. Input is supplied as an url parameter `email`.

## Tests
This project include unit tests. To run tests, node modules need to be installed so the test related packages can be installed locally. Run ```npm install``` to install dependencies then run ```npm test``` to run the test suit. A basic test suit has been added in this project, more coverage can be added as time permits.
