'use strict';

require('dotenv').load();
const neo4j = require('neo4j-driver').v1;
const driver = neo4j.driver(process.env.DB_HOST, neo4j.auth.basic(
    process.env.DB_USER,
    process.env.DB_PASSWORD
));
const session = driver.session();

module.exports.createUser = (user, callback) => {
    session.run(
        'MERGE (a:User {email: $email, name: $name, password: $password, isAdmin: $isAdmin}) RETURN a',
        {
            email: user['email'],
            name: user['name'],
            password: user['password'],
            isAdmin: user['isAdmin']
        }
    ).then(result => {
        callback(null, result.records[0].get(0).properties.email);
    }).catch(error => {
        callback(error);
    });
};

module.exports.getUser = (email, callback) => {
    session.run(
        'MATCH (a:User) WHERE a.email = $email RETURN a', {email: email}
    ).then(result => {
        if (result.records.length) {
            callback(null, result.records[0].get(0).properties);
        } else {
            callback('User not found');
        }
    }).catch(error => {
        callback(error);
    });
};

module.exports.listUsers = (excludeEmail, offset, limit, callback) => {
    session.run(
        'MATCH (a:User) WHERE a.email <> $email \
            RETURN a \
            ORDER BY a.name \
            SKIP $offset \
            LIMIT $limit \
        ', {
            email: excludeEmail || '',
            offset: offset || 0,
            limit: limit || 10
        }
    ).then(result => {
        var data = result.records;
        callback(null, data.map(item => item.get(0).properties));
    }).catch(error => {
        callback(error);
    });
};

module.exports.connectUser = (currentUser, userToConnect, callback) => {
    session.run(
        'MATCH (a:User),(b:User) WHERE a.email=$currentUser AND b.email=$userToConnect \
            CREATE UNIQUE (a)-[r:Follows]->(b) \
            RETURN r \
        ', {
            currentUser: currentUser,
            userToConnect: userToConnect
        }
    ).then(result => {
        callback(null, result);
    }).catch(error => {
        callback(error);
    });
};

module.exports.disconnectUser = (currentUser, userToDisconnect, callback) => {
    session.run(
        'MATCH (a:User)-[r:Follows]-(b:User) \
            WHERE a.email=$currentUser AND b.email=$userToDisconnect \
            DELETE r \
        ', {
            currentUser: currentUser,
            userToDisconnect: userToDisconnect
        }
    ).then(result => {
        callback(null, result);
    }).catch(error => {
        callback(error);
    });
};

module.exports.getConnections = (email, callback) => {
    session.run(
        'MATCH (a:User {email: $email}) \
            OPTIONAL MATCH (a)<-[r:Follows]-(b:User) \
            RETURN a.name, a.email, COLLECT({name: b.name, email: b.email}) AS followers \
            LIMIT 1 \
        ', {
            email: email
        }
    ).then(result => {
        callback(null, {
            name: result.records[0].get(0),
            email: result.records[0].get(1),
            followers: result.records[0].get(2).filter((item) => {
                return item.name && item.email ? true : false;
            })
        });
    }).catch(error => {
        callback(error);
    });
};
