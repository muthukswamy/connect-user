'use strict';

const express = require('express');
const async = require('async');
const auth = require('./auth');
const repo = require('./repo');
const bodyParser = require('body-parser');
const session = require('express-session');
require('dotenv').load();

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
}));

// App
app.get('/', (req, res) => {
    res.send({
        'POST: /register': 'User registration',
        'POST: /login': 'Login to the system to list users',
        'GET: /logout': 'Logout of the system',
        'GET: /list-users': 'List users except current user',
        'POST: /connect-user': 'Follow an existing user',
        'POST: /disconnect-user': 'Unfollow an user',
        'GET: /user-connections': 'Get a list of followers for the given user [Admin only]',
    });
});

app.post('/register', (req, res) => {
    if (!req.body.name || !req.body.email || !req.body.password) {
        res.status(500).send('Register requires name, email and passsword params');
    } else {
        async.waterfall([
            (cb) => {
                auth.encryptPassword(req.body.password, cb);
            },
            (password, cb) => {
                repo.createUser({
                    'email': req.body.email,
                    'name': req.body.name,
                    'password': password,
                    'isAdmin': req.body.name == 'admin' ? true : false
                }, cb);
            }
        ], (error, result) => {
            if (error) {
                res.status(500).send({error: error});
            } else {
                res.send({result: 'OK'});
            }
        });
    }
});


app.post('/login', (req, res) => {
    if (!req.body.email || !req.body.password) {
        res.status(500).send('Login requires email and passsword params');
    } else {
        async.waterfall([
            (cb) => {
                repo.getUser(req.body.email, cb);
            },
            (user, cb) => {
                auth.comparePassword(req.body.password, user.password, (error, result) => {
                    cb(error, user, result);
                });
            }
        ], (error, user, result) => {
            if (error) {
                res.status(500).send({error: error});
            } else {
                if (result) {
                    req.session.user = req.body.email;
                    req.session.isAdmin = user.isAdmin;
                    res.send({result: 'OK'});
                } else {
                    res.status(500).send({error: 'Invalid login details'});
                }
            }
        });
    }
});


app.get('/logout', (req, res) => {
    req.session.destroy();
    res.send({result: 'OK'});
});


app.get('/list-users', auth.checkUser, (req, res) => {
    var perPage = 2;
    var page = parseInt(req.query.page) || 1;
    var offset = (page - 1) * perPage;

    repo.listUsers(req.session.user, offset, perPage, (error, result) => {
        if (error) {
            res.status(500).send({error: error});
        } else {
            res.send({
                users: result.map((item) => {
                    var {password, ...item} = item;
                    return item;
                })
            });
        }
    });
});

app.post('/connect-user', auth.checkUser, (req, res) => {
    repo.connectUser(req.session.user, req.body.email, (error, result) => {
        if (error) {
            res.status(500).send({error: error});
        } else {
            res.send({result: 'OK'});
        }
    });
});

app.post('/disconnect-user', auth.checkUser, (req, res) => {
    repo.disconnectUser(req.session.user, req.body.email, (error, result) => {
        if (error) {
            res.status(500).send({error: error});
        } else {
            res.send({result: 'OK'});
        }
    });
});

app.get('/user-connections', auth.checkAdmin, (req, res) => {
    repo.getConnections(req.query.email, (error, result) => {
        if (error) {
            res.status(500).send({error: error});
        } else {
            res.send({result: result});
        }
    });
});

module.exports = app;
