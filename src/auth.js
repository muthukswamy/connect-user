'use strict';

const bcrypt = require('bcrypt');

exports.encryptPassword = (password, callback) => {
    bcrypt.genSalt(10, (error, salt) => {
        if (error) {
            return callback(error);
        }

        bcrypt.hash(password, salt, (error, hash) => {
            return callback(error, hash);
        });
    });
};

exports.comparePassword = (plainPass, hashword, callback) => {
    bcrypt.compare(plainPass, hashword, (error, isPasswordMatch) => {
        return error == null ? callback(null, isPasswordMatch) : callback(error);
    });
};

exports.checkUser = (req, res, next) => {
    if (req.session && req.session.user) {
        return next();
    } else {
        return res.status(401).send({error: 'Unauthorized'});
    }
};

exports.checkAdmin = (req, res, next) => {
    if (req.session && req.session.user && req.session.isAdmin) {
        return next();
    } else {
        return res.status(401).send({error: 'Unauthorized'});
    }
};
